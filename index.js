const swapi = require('swapi-node');

const runApi = () => {
    swapi.get('https://swapi.dev/api/films/')
        .then((res) => {
            Object.values(res.results).map(async films => {
                let buildFilms = {
                    name: films.title,
                    planets: await buildPlanets(films.planets),
                    people: await buildCharacters(films.characters),
                    starships: await buildVehicles(films.vehicles)
                }
                console.log(buildFilms)
            })
        })
        .catch((error) => console.log(error))
}

runApi();

const cacheStart = []

const formatUrl = (data) => {
    return data.split('http').join('https')
};

const cacheSystem = async (url) => {
    if (typeof cacheStart[url] === 'undefined') {
        cacheStart[url] = await swapi.get(url)
    }

    return cacheStart[url]
}

const setPlanet = (data) => {
    return {
        name: data.name,
        terrain: data.terrain,
        gravity: data.gravity,
        diameter: data.diameter,
        population: data.population
    }
}

const buildPlanets = async (planets) => {
    let dataPlanets = []
    await Promise.all(
        planets.map(async planet => {
            let url = formatUrl(planet)
            let infoPlanet  = await cacheSystem(url)
            dataPlanets.push(setPlanet(infoPlanet))
        })
    )
    return dataPlanets
}

const setCharacter = (data) => {
    return {
        name: data.name,
        gender: data.gender,
        hair_color: data.hair_color,
        skin_color: data.skin_color,
        eye_color: data.eye_color,
        height: data.height,
        homeworld: data.home_world,
        species: data.specie
    }
}

const buildCharacters = async (characters) => {
    let dataCharacters = []
    await Promise.all(
        characters.map(async Character => {
            let url = formatUrl(Character)
            let infoCharacter = await cacheSystem(url)
            let urlHomeWorld = formatUrl(infoCharacter.homeworld)

            let home_world = await cacheSystem(urlHomeWorld)
            infoCharacter.home_world = home_world.name

            if (infoCharacter.species.length == 0) {
                infoCharacter.specie = []
            } else {
                infoCharacter.specie = await buildSpecies(infoCharacter.species)
            }

            dataCharacters.push(setCharacter(infoCharacter))
        })
    )
    return dataCharacters
}

const setSpecies = (data) => {
    return {
        name: data.name,
        lenguage: data.language,
        average_height: data.average_height
    }
}

const buildSpecies = async (species) => {
    let dataSpecies
    await Promise.all(
        species.map(async specie => {
            let url = formatUrl(specie)
            let infoSpecie = await cacheSystem(url)
            dataSpecies = setSpecies(infoSpecie)
        })
    )
    return dataSpecies
}

const setVehicles = (data) => {
    return {
        name: data.name,
        model: data.model,
        manufacturer: data.manufacturer,
        passengers: data.passengers
    }
}

const buildVehicles = async (vehicles) => {
    let dataVehicles = []
    let bigger = []
    await Promise.all(
        vehicles.map(async vehicle => {
            let url = formatUrl(vehicle)
            let infoVehicle = await cacheSystem(url)
            bigger.push(parseInt(infoVehicle.passengers))
            order = bigger.sort((a,b)=>b-a)

            if (infoVehicle.passengers == order[0]){
                dataVehicles = setVehicles(infoVehicle)
            }
        })
    )
    return dataVehicles
}